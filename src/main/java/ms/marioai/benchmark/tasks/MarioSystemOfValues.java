package ms.marioai.benchmark.tasks;


public class MarioSystemOfValues extends SystemOfValues
{
    final public int distance = 1;
    final public int win = 1;
    final public int mode = 1;
    final public int coins = 1;
    final public int hiddenItems = 1;
    final public int flowerFire = 1;  // not used for now
    final public int kills = 1;
    final public int killedByFire = 1;
    final public int killedByShell = 1;
    final public int killedByStomp = 1;
    final public int timeLeft = 1;
    final public int hiddenBlocks = 1;

    public interface timeLengthMapping
    {
        final public static int TIGHT = 10;
        final public static int MEDIUM = 20;
        final public static int FLEXIBLE = 30;
    }
}
