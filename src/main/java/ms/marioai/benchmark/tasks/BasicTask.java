package ms.marioai.benchmark.tasks;

import ms.marioai.agents.Agent;
import ms.marioai.benchmark.mario.engine.GlobalOptions;
import ms.marioai.benchmark.mario.environments.Environment;
import ms.marioai.benchmark.mario.environments.MarioEnvironment;
import ms.marioai.tools.EvaluationInfo;
import ms.marioai.tools.MarioAIOptions;
import ms.marioai.utils.statistics.StatisticalSummary;

import java.util.Vector;


public class BasicTask implements Task
{
protected final static Environment environment = MarioEnvironment.getInstance();
private Agent agent;
protected MarioAIOptions options;
private long COMPUTATION_TIME_BOUND = 42; // stands for prescribed  FPS 24.
private String name = getClass().getSimpleName();
private EvaluationInfo evaluationInfo;

private Vector<StatisticalSummary> statistics = new Vector<StatisticalSummary>();

public Agent getAgent() 
{
    return agent;
}

public BasicTask(MarioAIOptions marioAIOptions)
{
    this.setOptionsAndReset(marioAIOptions);
}

/**
 * @param repetitionsOfSingleEpisode
 * @return boolean flag whether controller is disqualified or not
 */
public boolean runSingleEpisode(final int repetitionsOfSingleEpisode)
{
    long c = System.currentTimeMillis();
    
    for (int r = 0; r < repetitionsOfSingleEpisode; ++r) {
        this.reset();
        while (!environment.isLevelFinished()) {
            environment.tick();
            
            if (!GlobalOptions.isGameplayStopped) {
                c = System.currentTimeMillis();
                agent.integrateObservation(environment);
                agent.giveIntermediateReward(environment.getIntermediateReward());

                boolean[] action = agent.getAction();
                
                //if (System.currentTimeMillis() - c > COMPUTATION_TIME_BOUND)
                //    return false;
                
                //  System.out.println("action = " + Arrays.toString(action));
                //  environment.setRecording(GlobalOptions.isRecording);
                
                environment.performAction(action);
            }
        }
        environment.closeRecorder(); //recorder initialized in environment.reset
        environment.getEvaluationInfo().setTaskName(name);
        
        this.evaluationInfo = environment.getEvaluationInfo().clone();
    }

    return true;
}

public Environment getEnvironment()
{
    return environment;
}

public int evaluate(Agent controller)
{
    return 0;
}

public void setOptionsAndReset(MarioAIOptions options)
{
    this.options = options;
    reset();
}

public void setOptionsAndReset(final String options)
{
    this.options.setArgs(options);
    reset();
}

public void doEpisodes(int amount, boolean verbose, final int repetitionsOfSingleEpisode) {
    for (int j = 0; j < EvaluationInfo.numberOfElements; j++)
        statistics.addElement(new StatisticalSummary());
    
    for (int i = 0; i < amount; ++i) {
        agent.beginEpisode();
        this.reset();
        
        this.runSingleEpisode(repetitionsOfSingleEpisode);
        
        if (verbose)
            System.out.println(environment.getEvaluationInfoAsString());

        for (int j = 0; j < EvaluationInfo.numberOfElements; j++)
            statistics.get(j).add(environment.getEvaluationInfoAsInts()[j]);
        
        agent.episodeEnd();
    }

    //System.out.println(statistics.get(3).toString());
}

public boolean isFinished()
{
    return false;
}

public void reset()
{
    agent = options.getAgent();
    environment.reset(options);
    agent.reset();
    agent.setObservationDetails(environment.getReceptiveFieldWidth(),
            environment.getReceptiveFieldHeight(),
            environment.getMarioEgoPos()[0],
            environment.getMarioEgoPos()[1]);
}

public String getName()
{
    return name;
}

public void printStatistics()
{
    System.out.println(evaluationInfo.toString());
}

public EvaluationInfo getEvaluationInfo()
{
    return evaluationInfo;
}

}
