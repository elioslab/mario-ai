package ms.marioai.agents.astar;

import ms.marioai.agents.Agent;
import ms.marioai.benchmark.mario.environments.Environment;
import ms.marioai.agents.astar.algo.AStarSimulator;
import ms.marioai.agents.astar.algo.sprites.Mario;
import ms.marioai.agents.controllers.BasicMarioAIAgent;

public class AStarAgent extends BasicMarioAIAgent implements Agent {
    protected boolean action[] = new boolean[Environment.numberOfKeys];
    protected String name = "AStarAgent";
    private AStarSimulator sim;
    private float lastX = 0;
    private float lastY = 0;

    public AStarAgent() { super("AStarAgent"); }
    
    public void reset() {
        action = new boolean[Environment.numberOfKeys];
        sim = new AStarSimulator();
    }

    public boolean[] getAction() {
    	long startTime = System.currentTimeMillis();

    	boolean[] ac = new boolean[6];
    	ac[Mario.KEY_RIGHT] = true;
    	ac[Mario.KEY_SPEED] = true;

        byte[][] scene = environment.getLevelSceneObservationZ(0);
    	float[] enemies = environment.getEnemiesFloatPos();
	float[] realMarioPos = environment.getMarioFloatPos();

        sim.advanceStep(action);   
	
        if (sim.levelScene.mario.x != realMarioPos[0] || sim.levelScene.mario.y != realMarioPos[1]) {
            // Stop planning when we reach the goal (just assume we're in the goal when we don't move)
            //if (realMarioPos[0] == lastX && realMarioPos[1] == lastY) return ac;

            if (sim.levelScene.verbose > 0) System.out.println(  "Real: " + realMarioPos[0] + " " + realMarioPos[1]
                                                               + " Est: " + sim.levelScene.mario.x + " " + sim.levelScene.mario.y 
                                                               + " Diff: " + (realMarioPos[0] - sim.levelScene.mario.x) + " " + (realMarioPos[1]-sim.levelScene.mario.y));
			
            // Set the simulator mario to the real coordinates (x and y) and estimated speeds (xa and ya)
            sim.levelScene.mario.x = realMarioPos[0];
            sim.levelScene.mario.xa = (realMarioPos[0] - lastX) * 0.89f;
            if (Math.abs(sim.levelScene.mario.y - realMarioPos[1]) > 0.1f)
                sim.levelScene.mario.ya = (realMarioPos[1] - lastY) * 0.85f;

            sim.levelScene.mario.y = realMarioPos[1];
	}
		
	// Update the internal world to the new information received
	sim.setLevelPart(scene, enemies);
        
	lastX = realMarioPos[0];
	lastY = realMarioPos[1];

        // This is the call to the simulator (where all the planning work takes place)
        action = sim.optimise();
        
        // Some time budgeting, so that we do not go over X ms in average.
        sim.timeBudget += 40 - (int)(System.currentTimeMillis() - startTime);
        
        return action;
    }

    public String getName() { return name; }
    public void setName(String Name) { this.name = Name; }
}
