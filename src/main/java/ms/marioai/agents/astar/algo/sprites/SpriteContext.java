package ms.marioai.agents.astar.algo.sprites;


public interface SpriteContext
{
    public void addSprite(Sprite sprite);
    public void removeSprite(Sprite sprite);
}
