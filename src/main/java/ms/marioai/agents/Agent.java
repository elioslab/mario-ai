
package ms.marioai.agents;

import ms.marioai.benchmark.mario.environments.Environment;

public interface Agent
{
    boolean[] getAction();

    void integrateObservation(Environment environment);

    void giveIntermediateReward(float intermediateReward);

    public void reset();

    public void setObservationDetails(int rfWidth, int rfHeight, int egoRow, int egoCol);

    public String getName();

    public void setName(String name);

    public void episodeEnd();
    
    public void beginEpisode();
    
    public void end();
    
    public void begin();
    
    public String print();
}
