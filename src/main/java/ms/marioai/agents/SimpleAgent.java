package ms.marioai.agents;

import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class SimpleAgent implements Agent
{
protected boolean Action[] = new boolean[Environment.numberOfKeys];
protected String Name = "SimpleAgent";

public void integrateObservation(int[] serializedLevelSceneObservationZ, int[] serializedEnemiesObservationZ, float[] marioFloatPos, float[] enemiesFloatPos, int[] marioState)
{
    //To change body of implemented methods use File | Settings | File Templates.
}

public boolean[] getAction()
{
    return new boolean[0];  //To change body of implemented methods use File | Settings | File Templates.
}

public void integrateObservation(Environment environment)
{
    //To change body of implemented methods use File | Settings | File Templates.
}

public void giveIntermediateReward(float intermediateReward)
{

}

public void reset()
{
    Action = new boolean[Environment.numberOfKeys];
    Action[Mario.KEY_RIGHT] = true;
    Action[Mario.KEY_SPEED] = true;
}

public void setObservationDetails(final int rfWidth, final int rfHeight, final int egoRow, final int egoCol)
{}

public boolean[] getAction(Environment observation)
{
    Action[Mario.KEY_SPEED] = Action[Mario.KEY_JUMP] = observation.isMarioAbleToJump() || !observation.isMarioOnGround();
    return Action;
}

public String getName() { return Name; }

public void setName(String Name) { this.Name = Name; }

@Override
public void episodeEnd() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
}

@Override
public void beginEpisode() { }

@Override
public void end() { }

@Override
public void begin() { }

@Override
public String print() { return "no info"; }
}

