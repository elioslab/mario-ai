/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ms.marioai.agents.controllers.students2016;

import ms.marioai.agents.Agent;
import ms.marioai.agents.controllers.BasicMarioAIAgent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class Sanetti extends BasicMarioAIAgent implements Agent
{
public Sanetti()
{
    super("Sanetti");
    reset();
}

boolean isEnemy(int pos1, int pos2)
{
    boolean val;
    int tmp = getEnemiesCellValue(pos1, pos2);
    if(tmp > 0)
       val = true;
    else val = false;          
    
    return val;
}

public int timer = 0;

public boolean[] getAction()
{
   boolean EnemyInRange; 
   
    
   int x = marioEgoRow;
   int y = marioEgoCol; 
   
     EnemyInRange  =
           isEnemy(x+3,y) || isEnemy(x+3,y+1) || isEnemy(x+3,y+2) || isEnemy(x+3,y+3) ||
           isEnemy(x+2,y) || isEnemy(x+2,y+1) || isEnemy(x+2,y+2) || isEnemy(x+2,y+3) ||
           isEnemy(x+1,y) || isEnemy(x+1,y+1) || isEnemy(x+1,y+2) || isEnemy(x+1,y+3) ||
           isEnemy(x+4,y) || isEnemy(x+4,y+1) || isEnemy(x+4,y+2) || isEnemy(x+4,y+3);             
   
   if (action[Mario.KEY_RIGHT])
   {
     action[Mario.KEY_RIGHT] = !EnemyInRange; 
     
     timer = 0;
   }
   else
   {
       if (timer == 10)
        action[Mario.KEY_RIGHT] = true;
       
       else
       {
         action[Mario.KEY_RIGHT] = false;
         timer++;
       }           
   }
   
   action[Mario.KEY_JUMP] = isMarioAbleToJump || !isMarioOnGround;
                   
   if (marioMode == 2)
   {
    if (action[Mario.KEY_SPEED])   
        action[Mario.KEY_SPEED] = false;
    else action[Mario.KEY_SPEED] = true;   
   }
   else
        action[Mario.KEY_SPEED] = false;
//  
  return action;
}

public void reset()
{
    //action = new boolean[Environment.numberOfKeys];
   action[Mario.KEY_RIGHT] = false;
   action[Mario.KEY_SPEED] = false;
}
}
