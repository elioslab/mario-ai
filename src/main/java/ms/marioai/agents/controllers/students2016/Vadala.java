package ms.marioai.agents.controllers.students2016;

import ms.marioai.agents.Agent;
import ms.marioai.agents.controllers.BasicMarioAIAgent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class Vadala extends BasicMarioAIAgent implements Agent
{
    int ostacolo1, ostacolo2;
    int enemy1, enemy2, enemy3, enemy4, enemy5; // enemy6, enemy7, enemy8, enemy9, enemy10, enemy11, enemy12, enemy13, enemy14;    
    public Vadala()
    {
        super("Vadala");
        reset();
    }

    public void reset()
    {
        action = new boolean[Environment.numberOfKeys];
    }

    public boolean nemico(int x, int y)
    {
        return true;
    }
    
    public boolean[] getAction()
    {
        enemy1 = getEnemiesCellValue(marioEgoRow, marioEgoCol+1);
        enemy2 = getEnemiesCellValue(marioEgoRow, marioEgoCol+2);
        enemy3 = getEnemiesCellValue(marioEgoRow, marioEgoCol+3);
        enemy4 = getEnemiesCellValue(marioEgoRow-1, marioEgoCol+1);
        enemy5 = getEnemiesCellValue(marioEgoRow+1, marioEgoCol+1);
//        enemy6 = getEnemiesCellValue(marioEgoRow-2, marioEgoCol+1);
//        enemy7 = getEnemiesCellValue(marioEgoRow-2, marioEgoCol+2);
//        enemy8 = getEnemiesCellValue(marioEgoRow-2, marioEgoCol+3);
//        enemy9 = getEnemiesCellValue(marioEgoRow-2, marioEgoCol+4);
//        enemy10 = getEnemiesCellValue(marioEgoRow-3, marioEgoCol+1);
//        enemy11 = getEnemiesCellValue(marioEgoRow-3, marioEgoCol+2);
//        enemy12 = getEnemiesCellValue(marioEgoRow-3, marioEgoCol+3);
//        enemy13 = getEnemiesCellValue(marioEgoRow-3, marioEgoCol+4);
//        enemy14 = getEnemiesCellValue(marioEgoRow-3, marioEgoCol+5);

        action[Mario.KEY_SPEED] = !action[Mario.KEY_SPEED];
        action[Mario.KEY_LEFT] = false;
        
        if(enemy1 == 0 && enemy2 == 0 && enemy3 == 0 && enemy4 == 0 && enemy5 == 0) // enemy6 + enemy7 + enemy8 + enemy9 + enemy10 + enemy11 + enemy12 + enemy13 + enemy14 == 0){
        {
            action[Mario.KEY_RIGHT] = true;
        }
        else
        {
            action[Mario.KEY_RIGHT] = false;
            action[Mario.KEY_LEFT] = true;
            action[Mario.KEY_JUMP] = isMarioAbleToJump;
            //action[Mario.KEY_SPEED] = isMarioAbleToShoot;
        }
        
                
        ostacolo1 = getReceptiveFieldCellValue(marioEgoRow, marioEgoCol+1);
        ostacolo2 = getReceptiveFieldCellValue(marioEgoRow, marioEgoCol+2);
        if (ostacolo1 != 0 || ostacolo2 != 0)
            if (ostacolo1 != -11)
                action[Mario.KEY_JUMP] = isMarioAbleToJump || !isMarioOnGround;

        return action;
    }
}