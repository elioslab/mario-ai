package ms.marioai.agents.controllers.students2016;

import ms.marioai.agents.Agent;
import ms.marioai.agents.controllers.BasicMarioAIAgent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.engine.sprites.Sprite;
import ms.marioai.benchmark.mario.environments.Environment;

public class Mollo extends BasicMarioAIAgent implements Agent
{
    int jumpTime = 0;
    
    public Mollo()
    {
        super("Mollo");
        reset();
    }

    public void reset()
    {
        action = new boolean[Environment.numberOfKeys];
    }

    public boolean[] getAction()
    {
        reset();
        int x = marioEgoRow;
        int y = marioEgoCol;
        
        if(isCreature(enemies[x][y+1]) || isCreature(enemies[x][y+2]))
        {
            if(isMarioAbleToShoot)
                action[Mario.KEY_SPEED] = true;
            else
            {
                action[Mario.KEY_RIGHT] = true;
                action[Mario.KEY_JUMP] = true;
            }
        }
        else if(getReceptiveFieldCellValue(x + 1, y) == 0)
            action[Mario.KEY_RIGHT] = true;
        else if(getReceptiveFieldCellValue(x + 1, y) != 0)
        {
            action[Mario.KEY_RIGHT] = true;
            action[Mario.KEY_JUMP] = true;
        }
        
        if(isCreature(enemies[x + 2][y]))
            action[Mario.KEY_LEFT] = true;
        
        if(action[Mario.KEY_JUMP])
            jumpTime++;
        if (jumpTime > 5)
        {
            jumpTime = 0;
            action[Mario.KEY_JUMP] = false;
        }
        return action;
    }
    
    private boolean isCreature(int c)
    {
        switch (c)
        {
            case Sprite.KIND_GOOMBA:
            case Sprite.KIND_RED_KOOPA:
            case Sprite.KIND_RED_KOOPA_WINGED:
            case Sprite.KIND_GREEN_KOOPA_WINGED:
            case Sprite.KIND_GREEN_KOOPA:
                return true;
        }
        return false;
    }
}