package ms.marioai.agents.controllers.students2016;

import ms.marioai.agents.Agent;
import ms.marioai.agents.controllers.BasicMarioAIAgent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class Viviani extends BasicMarioAIAgent implements Agent
{
    int jumpCounter = 0;
    
    public Viviani()
    {
        super("Viviani");
        reset();
    }

    public void reset()
    {
        action = new boolean[Environment.numberOfKeys];
        action[Mario.KEY_RIGHT] = true;
        //action[Mario.KEY_SPEED] = true;
    }
    
    private boolean EnemyDetector()
    {

        if ((getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow + 1, marioEgoCol) != 0 ||
            getEnemiesCellValue(marioEgoRow + 2, marioEgoCol) != 0 ||
            getEnemiesCellValue(marioEgoRow + 2, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow + 2, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow + 1, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow + 1, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow + 3, marioEgoCol) != 0 ||
            getEnemiesCellValue(marioEgoRow + 3, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow + 3, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow + 3, marioEgoCol + 3) != 0 ||
            getEnemiesCellValue(marioEgoRow + 4, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow + 3, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow + 1, marioEgoCol - 1) != 0 ||
            getEnemiesCellValue(marioEgoRow + 1, marioEgoCol - 2) != 0 ||
            getEnemiesCellValue(marioEgoRow + 2, marioEgoCol - 1) != 0 ||
            getEnemiesCellValue(marioEgoRow + 2, marioEgoCol - 2) != 0 ||
            getEnemiesCellValue(marioEgoRow + 3, marioEgoCol - 1) != 0 ||
            getEnemiesCellValue(marioEgoRow + 2, marioEgoCol + 3) != 0 ||
            getEnemiesCellValue(marioEgoRow + 2, marioEgoCol + 4) != 0 ||
            getEnemiesCellValue(marioEgoRow + 1, marioEgoCol + 3) != 0 ||
            getEnemiesCellValue(marioEgoRow + 1, marioEgoCol + 4) != 0 ||
            getEnemiesCellValue(marioEgoRow + 4, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow + 4, marioEgoCol + 2) != 0))
            return true;
        else
            return false;
    }
    
    private boolean CollisionDetector()
    {
        if ((getReceptiveFieldCellValue(marioEgoRow + 2, marioEgoCol + 1) == 0 &&
            getReceptiveFieldCellValue(marioEgoRow + 1, marioEgoCol + 1) == 0) ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 0)
            return true;
        else
            return false;
    }
    
    private boolean AirborneEnemyDetector()
    {
        if((getEnemiesCellValue(marioEgoRow + 1, marioEgoCol - 1) != 0 ||
            getEnemiesCellValue(marioEgoRow + 1, marioEgoCol - 2) != 0 ||
            getEnemiesCellValue(marioEgoRow + 1, marioEgoCol - 3) != 0 ||
            getEnemiesCellValue(marioEgoRow + 1, marioEgoCol - 4) != 0 ||
            getEnemiesCellValue(marioEgoRow + 2, marioEgoCol - 1) != 0 ||
            getEnemiesCellValue(marioEgoRow + 2, marioEgoCol - 2) != 0 ||
            getEnemiesCellValue(marioEgoRow + 2, marioEgoCol - 3) != 0 ||
            getEnemiesCellValue(marioEgoRow + 2, marioEgoCol - 4) != 0))
            return true;
        else
            return false;
    }   
    

    public boolean[] getAction()
    {
        // this Agent requires observation integrated in advance.
    if (CollisionDetector() && getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 1)
    {
        if (isMarioAbleToJump || (!isMarioOnGround && action[Mario.KEY_JUMP]))
        {
            action[Mario.KEY_JUMP] = true;
        }
        ++jumpCounter;
    }
    else if (EnemyDetector())
    {
        if (isMarioAbleToJump || (!isMarioOnGround && action[Mario.KEY_JUMP]))
        {
            action[Mario.KEY_JUMP] = true;
        }
        ++jumpCounter;
    }
    else
    {
        action[Mario.KEY_JUMP] = false;
        jumpCounter = 0;
    }
    
    /*if (AirborneEnemyDetector() && action[Mario.KEY_JUMP])
    {
        action[Mario.KEY_LEFT] = true;
        action[Mario.KEY_RIGHT] = false;
    }
    else
    {
        action[Mario.KEY_LEFT] = false;
    }*/

    if (jumpCounter > 10)
    {
        jumpCounter = 0;
        action[Mario.KEY_JUMP] = false;
    }

    return action;
}
}