/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ms.marioai.agents.controllers.students2016;

import ms.marioai.agents.Agent;
import ms.marioai.agents.controllers.BasicMarioAIAgent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

/**
 *
 * @author utente
 */
public class Assanti extends BasicMarioAIAgent implements Agent {
    
    public Assanti (){
    
        super("Assanti");
        reset();
    }
    
    public void reset()
    {
        action = new boolean[Environment.numberOfKeys];
    }
    
    /*Dopo un po' ho smesso perchè bisognerebbe implementare una macchina a stati praticamente infinita.
    Il punto debole dello scripting è che ci sarà sempre almeno un caso che non si è considerato.
    Esso inoltre richiede molto tempo e risorse di calcolo: avrei potuto eseguire dei for che spazzolassero
    tutto lo schermo che il giocatore può vedere per cercare nemici, ostacoli, blocchi "?" e monete e decidere
    come comportarmi di conseguenza. Un altro problema dello scripting è che non si può fare altro, nella
    pratica, tirare ad indovinare quello chè succederà. Più si cerca di generalizzare e più il codice si complica.
    Ho qui eseguito un primo tentativo di fare questo ma è chiaro che non sarà efficace: per capire meglio,
    si considera qui il seguente caso:
    -Se posso sparare al nemico lo faccio.
    -Se non posso lo evito.
    Il problema che alcuni nemici del gioco sono invulnerabili alle palle di fuoco (i pallottoli Bill per esempio).
    Ne deriva che si dovrebbe complicare ancora il codice andando a vedere in che stato è il giocatore e che nemici
    ci sono in giro.
    Un altro problema non semplice con lo scripting (ma risolvibile con A*) è la calibrazione del salto per eseguire
    uccisioni o saltare un burrone (quanto è lungo il burrone? Sono nella posizione giusta per iniziare il salto?)
    Devo prendere una rincorsa?).
    */
    public boolean[] getAction()
{
    
if(getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) == 0)
    action[Mario.KEY_RIGHT] = true;

if(action[Mario.KEY_JUMP] == true && isMarioOnGround == true)
    {
        action[Mario.KEY_JUMP] = false;
        return action;
    }

if(Mario.fire == true && action[Mario.KEY_SPEED] == true){
    action[Mario.KEY_SPEED] = false;
    return action;
}

if (Mario.fire == true)
{
    for (int i = 0; i <= 5; i++){
        if(getEnemiesCellValue(marioEgoRow, marioEgoCol + i) != 0 || getEnemiesCellValue(marioEgoRow + i, marioEgoCol + i) != 0){
            action[Mario.KEY_SPEED] = true;
            break;//return action;
        }
        else
            action[Mario.KEY_SPEED] = false;
        }   
}
else
{
    action[Mario.KEY_SPEED] = false;
    for (int i = 0; i <= 2; i++){
        if(getEnemiesCellValue(marioEgoRow, marioEgoCol + i) != 0 /*|| getEnemiesCellValue(marioEgoRow + i, marioEgoCol + i) != 0*/){
            action[Mario.KEY_JUMP] = true;
            break;
        }
    }
}


for (int i = 0; i < 5; i++){
    if(isMarioAbleToJump && getReceptiveFieldCellValue(marioEgoRow - i, marioEgoCol + 1) != 0)
    action[Mario.KEY_JUMP] = true;
    break;
    }

if(Mario.large == false && Mario.fire == false){
    action[Mario.KEY_SPEED] = false;
    for (int i = 0; i <= 5; i++){
        if(getEnemiesCellValue(marioEgoRow, marioEgoCol + i) != 0 || getEnemiesCellValue(marioEgoRow + 1, marioEgoCol + i) != 0 || getEnemiesCellValue(marioEgoRow + 2, marioEgoCol + i) != 0 || getEnemiesCellValue(marioEgoRow + 3, marioEgoCol + i) != 0 || getEnemiesCellValue(marioEgoRow + 4, marioEgoCol + i) != 0){
            action[Mario.KEY_JUMP] = true;
            break;
        }
    }
}

    
    

    return action;
}
    
    
    
    
}
