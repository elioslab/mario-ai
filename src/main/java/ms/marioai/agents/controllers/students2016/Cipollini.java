package ms.marioai.agents.controllers.students2016;

import ms.marioai.agents.Agent;
import ms.marioai.agents.controllers.BasicMarioAIAgent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class Cipollini extends BasicMarioAIAgent implements Agent
{
int trueJumpCounter = 0;
int trueSpeedCounter = 0;

public Cipollini()
{
    super("Cipollini");
    reset();
}

public void reset()
{
    action = new boolean[Environment.numberOfKeys];
    action[Mario.KEY_RIGHT] = true;
    action[Mario.KEY_SPEED] = true;
    trueJumpCounter = 0;
    trueSpeedCounter = 0;
}

private boolean DangerOfAny()
{

        if ((getReceptiveFieldCellValue(marioEgoRow + 2, marioEgoCol + 1) != 0 &&
            getReceptiveFieldCellValue(marioEgoRow + 1, marioEgoCol + 1) != 0) ||
            (getReceptiveFieldCellValue(marioEgoRow -1 , marioEgoCol + 1) != 0 &&
            getReceptiveFieldCellValue(marioEgoRow -2, marioEgoCol + 1) != 0) ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0)
            return true;
        else
            return false;
}

public boolean[] getAction()
{
    if(getEnemiesCellValue(marioEgoRow -1  , marioEgoCol +1) != 0){
        action[Mario.KEY_JUMP] = true;
        ++trueJumpCounter;
    }
    else{
    if(DangerOfAny() && getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 1)  
    {
        if (isMarioAbleToJump)
        {
            action[Mario.KEY_JUMP] = true;
        }
        ++trueJumpCounter;
    }
    else
    {
        action[Mario.KEY_JUMP] = false;
        trueJumpCounter = 0;
    }
    }
    if (trueJumpCounter > 16)
    {
        trueJumpCounter = 0;
        action[Mario.KEY_JUMP] = false;
    }
    
   
    return action;
}
}