package ms.marioai.agents.controllers.students2016;

import ms.marioai.agents.Agent;
import ms.marioai.agents.controllers.BasicMarioAIAgent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class Beccaria_2 extends BasicMarioAIAgent implements Agent
{
    long timer=0;
    
    public Beccaria_2()
    {
        super("Beccaria_2");
        reset();
    }

    public void reset()
    {
        action = new boolean[Environment.numberOfKeys];
    }
    
    private boolean DangerOfAny()
{

        if ((getReceptiveFieldCellValue(marioEgoRow + 2, marioEgoCol + 1) == 0 &&
            getReceptiveFieldCellValue(marioEgoRow + 1, marioEgoCol + 1) == 0) ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 3) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol - 1) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol - 2) != 0 ||
            getEnemiesCellValue(marioEgoRow+1, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow+1, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow+1, marioEgoCol + 3) != 0 ||
            getEnemiesCellValue(marioEgoRow+1, marioEgoCol - 1) != 0 ||
            getEnemiesCellValue(marioEgoRow+1, marioEgoCol - 2) != 0 ||
            getEnemiesCellValue(marioEgoRow-1, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow-1, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow-1, marioEgoCol + 3) != 0 ||
            getEnemiesCellValue(marioEgoRow-1, marioEgoCol - 1) != 0 ||
            getEnemiesCellValue(marioEgoRow-1, marioEgoCol - 2) != 0
                )
            return true;
        else
            return false;
}

    public boolean[] getAction()
    {
        if(action[Mario.KEY_RIGHT]==false)
           action[Mario.KEY_RIGHT] = true;
        
        if((action[Mario.KEY_JUMP] == true && timer >= 7) && DangerOfAny())
        {
            action[Mario.KEY_JUMP] = false;
            timer=0;
        }
        else
        {
            action[Mario.KEY_JUMP] = true;
            timer++;
        }
        
        if(isMarioAbleToShoot)
        {
            if(action[Mario.KEY_SPEED]==true)
            {
                action[Mario.KEY_SPEED]=false;
            }
            else
            {
                action[Mario.KEY_SPEED]=true;
            }
        }
        
        
//        if(DangerOfAny())
//        {
//            action[Mario.KEY_JUMP] = true;
//        }   
        return action;
    }
}