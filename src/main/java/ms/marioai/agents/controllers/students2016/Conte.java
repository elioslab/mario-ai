package ms.marioai.agents.controllers.students2016;

import ms.marioai.agents.Agent;
import ms.marioai.agents.controllers.BasicMarioAIAgent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;
import com.sun.org.apache.bcel.internal.generic.RET;

public class Conte extends BasicMarioAIAgent implements Agent
{
    boolean cond;
    int count=0;
    int count2=0;
    int jumpcount=0;
    int speedcount=0;
    float positionpre=0;

    public Conte(boolean cond, String s) {
        super("Mollo");
        this.cond = cond;
    }
    
    
    public Conte()
    {
        super("Conte");
        reset();
    }

    public void reset()
    {
        action = new boolean[Environment.numberOfKeys];
        
        
        
    }

    
    public boolean [] getAction()
    {
        if(cond)
        {
            if((int)positionpre==(int)marioFloatPos[0]) count++;
            else if(positionpre>marioFloatPos[0])
            {
                count2++;
            
            }
            
            //System.out.println(count);
            if(IsThereSomeEnemy())
            {
            //action[Mario.KEY_JUMP]=isMarioAbleToJump;
            action[Mario.KEY_RIGHT]=false;
            action[Mario.KEY_SPEED]=true;
            
            
            }
            else
            {
            action[Mario.KEY_SPEED]=true;
            action[Mario.KEY_RIGHT]=true;
            action[Mario.KEY_UP]=true;
            //action[Mario.KEY_JUMP]= isMarioAbleToJump;
            
            
            
            }
            
            positionpre = marioFloatPos[0];
            action[Mario.KEY_JUMP] = isMarioAbleToJump || !isMarioOnGround;
            
            //System.out.println(positionpre);
            //System.out.println(marioFloatPos[0]);
            if(count>20)
            {
              //  action[Mario.KEY_DOWN]=true;
                action[Mario.KEY_LEFT]=true;
                action[Mario.KEY_RIGHT]=false;
                //action[Mario.KEY_JUMP]=true;
            
            
            }
            if(count2>20) 
            {
                
             count=0;
             count2=0;
             action[Mario.KEY_LEFT]=false;
            }
            
            
            
        }
        else 
        {
            action[Mario.KEY_UP]=false;
        //action[Mario.KEY_DOWN]=false;
        action[Mario.KEY_LEFT] =false;
        //action[Mario.KEY_RIGHT]= false;
        action[Mario.KEY_SPEED] = false;
        //action[Mario.KEY_JUMP] = false;
        
        
        }
    if(getReceptiveFieldCellValue(marioEgoRow, marioEgoCol+2)==-11)
    {
        action[Mario.KEY_UP]=true;
        action[Mario.KEY_RIGHT]=true;
        action[Mario.KEY_JUMP]=true;
        action[Mario.KEY_SPEED]=true;
    }
        cond=!cond;
    return action;
    
    
    }
    
    private boolean IsThereSomethings()
{
    
    if(getReceptiveFieldCellValue(marioEgoRow, marioEgoCol+1)!=0||
                getReceptiveFieldCellValue(marioEgoRow, marioEgoCol+1)!=0||
                getReceptiveFieldCellValue(marioEgoRow+1, marioEgoCol+1)!=0||
                getReceptiveFieldCellValue(marioEgoRow+1, marioEgoCol+1)!=0||
                getReceptiveFieldCellValue(marioEgoRow+2, marioEgoCol+1)!=0||
                getReceptiveFieldCellValue(marioEgoRow+2, marioEgoCol+1)!=0)
            return true;
        
        else return false;
}
    
    private boolean IsThereSomeEnemy()
    {
      
        if(((getEnemiesCellValue(marioEgoRow, marioEgoCol+1))!=0)||
                ((getEnemiesCellValue(marioEgoRow, marioEgoCol+2))!=0)||
                ((getEnemiesCellValue(marioEgoRow, marioEgoCol+3))!=0)||
                ((getEnemiesCellValue(marioEgoRow+1, marioEgoCol+1))!=0)||
                ((getEnemiesCellValue(marioEgoRow+1, marioEgoCol+2))!=0)||
                ((getEnemiesCellValue(marioEgoRow+1, marioEgoCol+3))!=0)||
                ((getEnemiesCellValue(marioEgoRow+1, marioEgoCol+4))!=0)||
                ((getEnemiesCellValue(marioEgoRow+2, marioEgoCol+1))!=0)||
                ((getEnemiesCellValue(marioEgoRow+2, marioEgoCol+2))!=0)||
                ((getEnemiesCellValue(marioEgoRow+2, marioEgoCol+3))!=0)||
                ((getEnemiesCellValue(marioEgoRow+2, marioEgoCol+4))!=0)||
                ((getEnemiesCellValue(marioEgoRow-1, marioEgoCol-1))!=0)||
                ((getEnemiesCellValue(marioEgoRow-1, marioEgoCol-2))!=0)||
                ((getEnemiesCellValue(marioEgoRow-1, marioEgoCol-3))!=0)||
                ((getEnemiesCellValue(marioEgoRow-1, marioEgoCol-4))!=0)||
                ((getEnemiesCellValue(marioEgoRow-2, marioEgoCol-1))!=0)||
                ((getEnemiesCellValue(marioEgoRow-2, marioEgoCol-2))!=0)||
                ((getEnemiesCellValue(marioEgoRow-2, marioEgoCol-3))!=0)||
                ((getEnemiesCellValue(marioEgoRow-2, marioEgoCol-4))!=0)) return true;
        
        else return false;
    
    
    
    }
    
    
    
    
}