package ms.marioai.agents.controllers.students2016;

import ms.marioai.agents.Agent;
import ms.marioai.agents.controllers.BasicMarioAIAgent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class PiaDaum extends BasicMarioAIAgent implements Agent
{
    
    public PiaDaum()
    {
        super("PiaDaum");
        reset();
    }

    public void reset()
    {
        action = new boolean[Environment.numberOfKeys];
    }

    private boolean DangerOfAny()
{

        if ((getReceptiveFieldCellValue(marioEgoRow + 3, marioEgoCol + 1) == 0 &&
            getReceptiveFieldCellValue(marioEgoRow + 2, marioEgoCol + 1) == 0) ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 0 ||   
            getReceptiveFieldCellValue(marioEgoRow-1, marioEgoCol + 1) != 0 ||
            getReceptiveFieldCellValue(marioEgoRow-2, marioEgoCol + 1) != 0 ||   
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 3) != 0 ||
            getEnemiesCellValue(marioEgoRow -1, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow -1, marioEgoCol + 3) != 0 ||
            getEnemiesCellValue(marioEgoRow -1, marioEgoCol + 4) != 0  
                
                
                )
            return true;
        else
            return false;
}
    
    public boolean[] getAction()
    {
        
        action[Mario.KEY_RIGHT] = true;
        
        
        
        if( DangerOfAny() && isMarioAbleToJump  )
        {
               
            action[Mario.KEY_RIGHT] = true;
            action[Mario.KEY_SPEED] = true;
            action[Mario.KEY_JUMP] = true; 
            
            
            
        }
        
        else if (getEnemiesCellValue(marioEgoRow + 2  , marioEgoCol+4 ) != 0 || getEnemiesCellValue(marioEgoRow + 2  , marioEgoCol+6 ) !=0  )
        {
            action[Mario.KEY_SPEED] = false;
            
            
            
        }
       
        
         else if ( getReceptiveFieldCellValue(marioEgoRow + 1 , marioEgoCol) == 1 && isMarioAbleToJump )
        {
            action[Mario.KEY_JUMP] = false;
            action[Mario.KEY_SPEED] = true;
            action[Mario.KEY_RIGHT] = true;
        }
         
        else
        {
           
            action[Mario.KEY_SPEED] = action[Mario.KEY_JUMP] = isMarioAbleToJump || !isMarioOnGround;
            
        }
        return action;
    }
}