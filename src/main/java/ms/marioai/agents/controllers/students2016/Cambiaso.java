package ms.marioai.agents.controllers.students2016;

import ms.marioai.agents.Agent;
import ms.marioai.agents.controllers.BasicMarioAIAgent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Karakovskiy, sergey.karakovskiy@gmail.com
 * Date: Apr 8, 2009
 * Time: 4:03:46 AM
 */

public class Cambiaso extends BasicMarioAIAgent implements Agent
{
int trueJumpCounter = 0;
int trueSpeedCounter = 0;

public Cambiaso()
{
    super("Cambiaso");
    reset();
}

public void reset()
{
    //action = new boolean[Environment.numberOfKeys];
    
    action[Mario.KEY_RIGHT] = true;
    
    trueJumpCounter = 0;
    trueSpeedCounter = 0;
    
}

private boolean DangerOfAny()
{

        if ((getReceptiveFieldCellValue(marioEgoRow + 2, marioEgoCol + 1) == 0 &&
            getReceptiveFieldCellValue(marioEgoRow + 1, marioEgoCol + 1) == 0) ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0 || getEnemiesCellValue(marioEgoRow+1, marioEgoCol + 2) != 0)
            return true;
        else
            return false;
}

private boolean isenemy(){
    if((getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0 || getEnemiesCellValue(marioEgoRow+1, marioEgoCol + 2) != 0))
            return true;
    else
        return false;
}

public boolean[] getAction()
{
    // this Agent requires observation integrated in advance.
    if(isenemy() && isMarioAbleToShoot)
        action[Mario.KEY_SPEED] = true;
    else     action[Mario.KEY_SPEED] = false;
    
    if(DangerOfAny()){

        if (isMarioAbleToJump || (!isMarioOnGround && action[Mario.KEY_JUMP]))
        {
            action[Mario.KEY_JUMP] = true;
        }
    
        ++trueJumpCounter;
    }
    else
    {
        action[Mario.KEY_SPEED] = false;
        action[Mario.KEY_JUMP] = false;
        trueJumpCounter = 0;
    }

    if (trueJumpCounter > 16)
    {
        trueJumpCounter = 0;
        action[Mario.KEY_JUMP] = false;
    }
    
//    if(getReceptiveFieldCellValue(marioEgoRow+1, marioEgoCol+1)== 0)
////        while(!(getReceptiveFieldCellValue(marioEgoRow+2, marioEgoCol)!= 0))
//        {    
//            action[Mario.KEY_SPEED] = true;
//            action[Mario.KEY_DOWN] = true;
//        }
    
    return action;

}

}