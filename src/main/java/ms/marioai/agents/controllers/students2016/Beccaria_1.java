package ms.marioai.agents.controllers.students2016;

import ms.marioai.agents.Agent;
import ms.marioai.agents.controllers.BasicMarioAIAgent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class Beccaria_1 extends BasicMarioAIAgent implements Agent
{
    long timer=0;
    long timer2=0;
        
    public Beccaria_1()
    {
        super("Beccaria_1");
        reset();
    }

    public void reset()
    {
        action = new boolean[Environment.numberOfKeys];
    }
    
    //controllo la presenza di ostacoli
    private boolean OstacoliDiFronte()
    {
        int temp=0;
        
        for(int i=1;i<3;i++)
        {
            temp+=getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + i);
        }
        
        if(temp==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    //controllo la presenza di nemici di fronte
    private boolean NemiciDiFronte()
    {
        int temp=0;
        
        for(int i=1;i<7;i++)
        {
            temp+=getEnemiesCellValue(marioEgoRow, marioEgoCol + i);
            temp+=getEnemiesCellValue(marioEgoRow + 1, marioEgoCol + i);
            temp+=getEnemiesCellValue(marioEgoRow - 1, marioEgoCol + i);
        }
        
        if(temp==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    //controllo la presenza di nemici in alto
    private boolean NemiciInAlto()
    {
        int temp=0;
        
        for(int i=1;i<5;i++)
        {
            temp+=getEnemiesCellValue(marioEgoRow - 2, marioEgoCol + i);
            temp+=getEnemiesCellValue(marioEgoRow - 3, marioEgoCol + i);
            temp+=getEnemiesCellValue(marioEgoRow - 4, marioEgoCol + i);
            temp+=getEnemiesCellValue(marioEgoRow - 5, marioEgoCol + i);
        }
        
        if(temp==0)
        {
            return false;
        }
        else 
        {
            return true;
        }
    }
    
    //effettua il salto
    private void jumpAct(int time)
    {
        if(time<0)
            time=0;
        if(time>7)
            time=7;
        
        if(isMarioAbleToJump)
        {
            action[Mario.KEY_JUMP] = true;
            timer++;
        }
        else if(timer > time)
        {
            action[Mario.KEY_JUMP] = false;
            timer=0;
        }
        else
        {
            timer++;
        }
    }
    
    //effettua sparo
    private void fireAct()
    {
        if(isMarioAbleToShoot)
        {
            action[Mario.KEY_SPEED] = true;
        }
        else
        {
            action[Mario.KEY_SPEED] = false;
        }
    }
    
    public boolean[] getAction()
    {
        //
        //fireAct();
        
        //
        if(action[Mario.KEY_RIGHT]==false)
           action[Mario.KEY_RIGHT] = true;
        
        if(OstacoliDiFronte())
        {
            jumpAct(7);
        }
        
        if(NemiciDiFronte())
        {
            jumpAct(3);
            fireAct();
        }
        
        if(NemiciInAlto())
        {
            action[Mario.KEY_LEFT]=true;
            action[Mario.KEY_RIGHT]=false;
        }
        else
        {
            action[Mario.KEY_LEFT]=false;
            action[Mario.KEY_RIGHT]=true;
        }
        
//        if(action[Mario.KEY_LEFT]==true)
//        {
//            action[Mario.KEY_RIGHT]=false; 
//        }
//        if((action[Mario.KEY_JUMP] == true && timer >= 3) && OstacoliDiFronte() && )
//        {
//            action[Mario.KEY_JUMP] = false;
//            timer=0;
//        }
//        else
//        {
//            action[Mario.KEY_JUMP] = true;
//            timer++;
//        }
//        
//        if(isMarioAbleToShoot)
//        {
////            if(action[Mario.KEY_SPEED]==true)
////            {
////                action[Mario.KEY_SPEED]=false;
////            }
////            else
////            {
//            action[Mario.KEY_SPEED]=true;
//            //}
//        }else
//        {
//            action[Mario.KEY_SPEED]=false;
//        }
//        
//        if(HighDangerOfAny())
//        {
//            action[Mario.KEY_JUMP]=false;
//        }
//        
//        if(VeryHighDangerOfAny() && timer2<100)
//        {
//            action[Mario.KEY_LEFT]=true;
//            action[Mario.KEY_RIGHT]=false;
//            timer2++;
//        }
//        else
//        {
//            action[Mario.KEY_LEFT]=false;
//            action[Mario.KEY_RIGHT]=true;
//            timer2=0;
//        }
//        
        
//        if(
//                getEnemiesCellValue(marioEgoRow+2,marioEgoCol+2)!=0 ||
//                getEnemiesCellValue(marioEgoRow+3,marioEgoCol+2)!=0 ||
//                getEnemiesCellValue(marioEgoRow+4,marioEgoCol+2)!=0
//          )
//        {
//            int temp1=getEnemiesCellValue(marioEgoRow+2,marioEgoCol+2);
//            int temp2=getEnemiesCellValue(marioEgoRow+3,marioEgoCol+2);
//            int temp3=getEnemiesCellValue(marioEgoRow+4,marioEgoCol+2);
//            
//            action[Mario.KEY_LEFT]=true;
//        }
        return action;
    }
}