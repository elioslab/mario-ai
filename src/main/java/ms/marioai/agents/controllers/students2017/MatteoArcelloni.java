package ms.marioai.agents.controllers.students2017;

import ms.marioai.agents.controllers.*;
import ms.marioai.agents.Agent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class MatteoArcelloni extends BasicMarioAIAgent implements Agent
{
    int trueJumpCounter = 0;
    int trueSpeedCounter = 0;
    int trueWaitCounter = 0;

    public MatteoArcelloni() {
        super("NomeCognome");
        reset();
    }

    public void reset() {
        action = new boolean[Environment.numberOfKeys];
        action[Mario.KEY_RIGHT] = false;
        action[Mario.KEY_SPEED] = false;
        trueJumpCounter = 0;
        trueSpeedCounter = 0;
    }

    private boolean DangerOfAny() {
        if ((getReceptiveFieldCellValue(marioEgoRow + 2, marioEgoCol + 1) == 0 &&
            getReceptiveFieldCellValue(marioEgoRow + 1, marioEgoCol + 1) == 0) ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0)
            return true;
        else
            return false;
    }

    private int EnemyisNear()
    {
        for(int c=0; c<5;c++)
        {
            for(int d=0; d<5;d++)
            {
                if(getEnemiesCellValue(marioEgoRow-d, marioEgoCol + c) != 0)
                {
                    if(d<marioEgoRow){return 2;}
                    else if(c==1) return 3;
                    else if(c==0 && d==0)return 4;
                    else return 1;
                }
            }
        }
        return 0;
    }
    private boolean BlockisNear()
    {
        if(getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
                getReceptiveFieldCellValue(marioEgoRow, marioEgoCol) != 0) 
        {return true;}
        else return false;
    }  
    private boolean HoleisNear()
    {
        if(getReceptiveFieldCellValue(marioEgoRow+1, marioEgoCol + 1) == 0 &&
                getReceptiveFieldCellValue(marioEgoRow+1, marioEgoCol + 2) == 0 
                
                ) 
        {return true;}
        else return false;
    }
    private void Jump()
    {
        if(isMarioOnGround)
        {
            if(trueJumpCounter == 0)
            {
                action[Mario.KEY_JUMP]=true;
                trueJumpCounter = 2;
            }
        }
        
    }
    
    private void Wait()
    {
         if(trueWaitCounter == 0)
         {
             action[Mario.KEY_RIGHT]= false;
             trueWaitCounter++;
         }
         
    }
    
    public boolean[] getAction() {
        if(trueSpeedCounter>0)
        {
            trueSpeedCounter--;
            action[Mario.KEY_SPEED]=false;
        }
        
        action[Mario.KEY_SPEED]=false;
        
        if(trueJumpCounter>0) trueJumpCounter--;
        //action[Mario.KEY_RIGHT]= true;
        if(action[Mario.KEY_JUMP] == true && isMarioOnGround == true)
        {   
            action[Mario.KEY_JUMP] = false;
            return action;
        }
        
        if(trueWaitCounter>30)
        {
            action[Mario.KEY_RIGHT]= true;
            trueWaitCounter = 0;
        }
        
        if(BlockisNear() || HoleisNear() || (EnemyisNear()==4) || EnemyisNear()==3)
        {
             Jump();   
        }
        //nemico in un tile avanti
        else if(EnemyisNear()==1)
        {
            Wait();
        } 
        //nemico in un tile avanti ma sopra
        else if (EnemyisNear()==2)
        {
            action[Mario.KEY_RIGHT]= true;
        }
        else 
        {
            action[Mario.KEY_RIGHT]= true;
        }
        
        if(isMarioAbleToShoot)
        {
            if(trueSpeedCounter == 0)
            {
                trueSpeedCounter = 2;
                action[Mario.KEY_SPEED]=true;
            }
            
        }
        
        return action;
    }

    @Override
    public void end() {
    }

    @Override
    public void begin() {
    }
}