package ms.marioai.agents.controllers.students2017;

import ms.marioai.agents.controllers.*;
import ms.marioai.agents.Agent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class FabioBosica_Mushrooms extends BasicMarioAIAgent implements Agent
{
    int trueJumpCounter = 0;
    int trueSpeedCounter = 0;
    int backwardCounter = 0;
    int stoppedCounter = 0;
    int goBackCounter = 60;
    int backJumpCounter = 0;
    float lastPos=0;
     
    public FabioBosica_Mushrooms() {
        super("FabioBosica");
        reset();
    }

    public void reset() {
        action = new boolean[Environment.numberOfKeys];
        action[Mario.KEY_RIGHT] = false;
        action[Mario.KEY_SPEED] = false;
        trueJumpCounter = 0;
        trueSpeedCounter = 0;
        backwardCounter = 0;
        stoppedCounter = 0;
        goBackCounter = 60;
        backJumpCounter = 0;
        lastPos=0;
    }

    private boolean DangerOfAny() {
        if ( action[Mario.KEY_LEFT] == false){          
            if (
                    (getReceptiveFieldCellValue(marioEgoRow + 1, marioEgoCol + 1) == 0) ||
                    
                    (getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0 &&
                    getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 22) ||    
                    
                    (getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 22 &&
                    getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 0) ||
                    
                    getEnemiesCellValue(marioEgoRow-1, marioEgoCol + 1) >= 95  ||
                    getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||     
                    getEnemiesCellValue(marioEgoRow-1, marioEgoCol + 2) != 0 ||   
                    getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0)
            {
                return true;
            }            
        }
        if ( action[Mario.KEY_RIGHT] == false){       
            if (
                    (getReceptiveFieldCellValue(marioEgoRow + 2, marioEgoCol - 1) == 0 &&
                    getReceptiveFieldCellValue(marioEgoRow + 1, marioEgoCol - 1) == 0) ||
                    getReceptiveFieldCellValue(marioEgoRow, marioEgoCol - 1) != 0 ||
                    getReceptiveFieldCellValue(marioEgoRow, marioEgoCol - 2) != 0 ||
                    getEnemiesCellValue(marioEgoRow, marioEgoCol - 1) != 0 ||
                    getEnemiesCellValue(marioEgoRow, marioEgoCol - 2) != 0)
            {
                return true;
            }
        }        
        return false;        
    }

    private boolean isForewardFree() {
        if (
                (
                getReceptiveFieldCellValue(marioEgoRow + 2, marioEgoCol + 1) == 0 &&
                getReceptiveFieldCellValue(marioEgoRow + 1, marioEgoCol + 1) == 0) ||
                getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
                getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 0 ||
                
                getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
                getEnemiesCellValue(marioEgoRow-1, marioEgoCol + 1) != 0 ||
                getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    private boolean isFarFree() {
        if (
                getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0 ||
                getEnemiesCellValue(marioEgoRow-1, marioEgoCol + 2) != 0 ||
                getEnemiesCellValue(marioEgoRow+1, marioEgoCol + 1) != 1 ||
                getEnemiesCellValue(marioEgoRow+1, marioEgoCol + 3) != 1 ||
                getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) == 1  
                ) 
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    private boolean isJumpingForewardFree() {
        if (
                getEnemiesCellValue(marioEgoRow-1, marioEgoCol+1) != 0 ||
                getEnemiesCellValue(marioEgoRow-1, marioEgoCol) != 0 ||
                getEnemiesCellValue(marioEgoRow-2, marioEgoCol+1) != 0            
            )           
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    private boolean isJumpingSuggested() {         
        if (     
                getEnemiesCellValue(marioEgoRow-2, marioEgoCol + 2) != 0 ||       
                getEnemiesCellValue(marioEgoRow-1, marioEgoCol + 1) != 0 || 
                getEnemiesCellValue(marioEgoRow-2, marioEgoCol + 1) != 0 ||
                getEnemiesCellValue(marioEgoRow-3, marioEgoCol    ) != 0          
                
                ||getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0  
                ||getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0 
                ||getEnemiesCellValue(marioEgoRow, marioEgoCol + 3) != 0 
                                
                ||getEnemiesCellValue(marioEgoRow+2, marioEgoCol+1) != 0      
                ||getEnemiesCellValue(marioEgoRow+3, marioEgoCol+2) != 0)
        {
             return false;
        }
        else
        {
            return true;
        }
    }

    private boolean isStopped() {   
        if ((int)lastPos ==(int)marioFloatPos[0])
        {
            stoppedCounter+=4; 
        }
        else if ( stoppedCounter>=0)
        {
            stoppedCounter--;          
        }
        
        lastPos = marioFloatPos[0] ;
        
        if ( stoppedCounter >= 200 )
        {
            return true;
        }

        return false;
    }
     
    private boolean isBelowFree() {
        if ( action[Mario.KEY_RIGHT] == true){
            if (
                    (getReceptiveFieldCellValue(marioEgoRow+2, marioEgoCol ) == 1 && 
                    getReceptiveFieldCellValue(marioEgoRow+2, marioEgoCol + 1) <= 1 &&
                    getReceptiveFieldCellValue(marioEgoRow+1, marioEgoCol + 1) <= 1)    ||
                    (getReceptiveFieldCellValue(marioEgoRow+3, marioEgoCol ) == 1 &&
                    getReceptiveFieldCellValue(marioEgoRow+3, marioEgoCol + 1) <= 1 &&
                    getReceptiveFieldCellValue(marioEgoRow+2, marioEgoCol + 1) <= 1 &&
                    getReceptiveFieldCellValue(marioEgoRow+1, marioEgoCol + 1) <= 1))
            {
                return true;    
            }
        }
        if ( action[Mario.KEY_LEFT] == true){
            if (
                    (getReceptiveFieldCellValue(marioEgoRow+2, marioEgoCol ) == 1 && 
                    getReceptiveFieldCellValue(marioEgoRow+2, marioEgoCol - 1) <= 1 &&
                    getReceptiveFieldCellValue(marioEgoRow+1, marioEgoCol - 1) <= 1)    ||
                    (getReceptiveFieldCellValue(marioEgoRow+3, marioEgoCol ) == 1 &&
                    getReceptiveFieldCellValue(marioEgoRow+3, marioEgoCol - 1) <= 1 &&
                    getReceptiveFieldCellValue(marioEgoRow+2, marioEgoCol - 1) <= 1 &&
                    getReceptiveFieldCellValue(marioEgoRow+1, marioEgoCol - 1) <= 1))
            { 
                return true;      
            }
        }       
        return false;
    }
    
    public boolean[] getAction() {
       
        if (isStopped() && goBackCounter>0)
        {  
            action[Mario.KEY_RIGHT] = false;
            action[Mario.KEY_LEFT] = true;
            --goBackCounter;
            
            if (goBackCounter <=0)
            {
                goBackCounter=60;
                stoppedCounter =0 ; 
                action[Mario.KEY_LEFT] = false;
                action[Mario.KEY_RIGHT] = true;
            }
        }
        else 
        {
            if (action[Mario.KEY_JUMP] == true) 
            {                         
                if (isBelowFree() && stoppedCounter<=40)       
                {
                    action[Mario.KEY_RIGHT] = false;
                    action[Mario.KEY_LEFT] = false;
                }
                else if (isJumpingSuggested() == false)
                {
                    action[Mario.KEY_RIGHT] = false;
                    action[Mario.KEY_LEFT ] = true;
                }
                else if (isJumpingForewardFree())
                {
                    action[Mario.KEY_LEFT] = false;
                    action[Mario.KEY_RIGHT] = true;
                }
                else
                {
                    action[Mario.KEY_RIGHT] = false;
                    action[Mario.KEY_LEFT] = true;   
                }                
            }
            else if (isForewardFree())
            {
                action[Mario.KEY_LEFT] = false;
                action[Mario.KEY_RIGHT] = true;   
                if (isFarFree())
                {
                    action[Mario.KEY_SPEED] = true;
                }
                else
                {
                    action[Mario.KEY_SPEED] = false;
                }       
            }
            else 
            {
                action[Mario.KEY_LEFT] = false;
                action[Mario.KEY_RIGHT] = false;
                action[Mario.KEY_SPEED] = false;
            }
        }
        if (DangerOfAny() ){ 
            if ((isMarioAbleToJump || (!isMarioOnGround && action[Mario.KEY_JUMP])) && isJumpingForewardFree())
            { 
                action[Mario.KEY_JUMP] = true;
            }
            ++trueJumpCounter;
           
            if (isMarioAbleToShoot && marioMode==2 )
            {
                action[Mario.KEY_SPEED ] = true;            
            }
            else
            {
                action[Mario.KEY_SPEED ] = false; 
            }        
        }       
        else 
        {
            if (stoppedCounter<=30)
            {
                action[Mario.KEY_JUMP] = false;
                trueJumpCounter = 0;
            }
        }

        if (action[Mario.KEY_LEFT ] == true )       
        {
            ++backwardCounter;
            if( backwardCounter >= 4)
            {
                action[Mario.KEY_LEFT ] = false; 
                backwardCounter = 0;
            }
        }       
          
        if (trueJumpCounter > 16) {
            trueJumpCounter = 0;
            action[Mario.KEY_JUMP] = false;
        }
        
        return action;
    }

    @Override
    public void end() {
    }

    @Override
    public void begin() {
    }
}