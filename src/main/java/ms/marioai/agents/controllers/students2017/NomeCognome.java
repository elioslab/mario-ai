package ms.marioai.agents.controllers.students2017;

import ms.marioai.agents.controllers.*;
import ms.marioai.agents.Agent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class NomeCognome extends BasicMarioAIAgent implements Agent
{
    int trueJumpCounter = 0;
    int trueSpeedCounter = 0;

    public NomeCognome() {
        super("NomeCognome");
        reset();
    }

    public void reset() {
        action = new boolean[Environment.numberOfKeys];
        action[Mario.KEY_RIGHT] = true;
        action[Mario.KEY_SPEED] = true;
        trueJumpCounter = 0;
        trueSpeedCounter = 0;
    }

    private boolean DangerOfAny() {
        if ((getReceptiveFieldCellValue(marioEgoRow + 2, marioEgoCol + 1) == 0 &&
            getReceptiveFieldCellValue(marioEgoRow + 1, marioEgoCol + 1) == 0) ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0)
            return true;
        else
            return false;
    }

    public boolean[] getAction() {
        if (DangerOfAny() && getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 1) {
            if (isMarioAbleToJump || (!isMarioOnGround && action[Mario.KEY_JUMP])) {
                action[Mario.KEY_JUMP] = true;
            }
            ++trueJumpCounter;
        }
        else {
        action[Mario.KEY_JUMP] = false;
        trueJumpCounter = 0;
        }

        if (trueJumpCounter > 16) {
            trueJumpCounter = 0;
            action[Mario.KEY_JUMP] = false;
        }
    
        return action;
    }

    @Override
    public void end() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void begin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}