package ms.marioai.agents.controllers.students2017;

import ms.marioai.agents.controllers.*;
import ms.marioai.agents.Agent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class Garrone extends BasicMarioAIAgent implements Agent
{
    int trueJumpCounter = 0;
    int trueSpeedCounter = 0;
    int timer =0;
    
    int posPolling = 0;   
    int count=0;
    
    public Garrone() {
        super("DavideGarrone");
        reset();
    }

    public void reset() {
        action = new boolean[Environment.numberOfKeys];
        action[Mario.KEY_RIGHT] = true;
        action[Mario.KEY_SPEED] = false;
        trueJumpCounter = 0;
        trueSpeedCounter = 0;
    }

    private boolean DangerOfAny() 
    {
        if ((getReceptiveFieldCellValue(marioEgoRow + 2, marioEgoCol + 1) == 0 &&
            getReceptiveFieldCellValue(marioEgoRow + 1, marioEgoCol + 1) == 0) ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0 ||
            getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0)
            return true;
              
        else
            return false;
               
    }
    
    private void AntiStop()  //se Mario sta fermo per troppo tempo, salta
    {
    count++;

        if (count >= 30)
        {
            posPolling = (int)marioFloatPos[0];
            count = 0;
        }
        if(posPolling - (int)marioFloatPos[0] == 0)
        {
            action[Mario.KEY_JUMP] = true;
        }
    }

    public boolean[] getAction() {
        if (DangerOfAny() == true && getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 1) 
        {
            if (isMarioAbleToJump || (isMarioOnGround == false && action[Mario.KEY_JUMP] == true)) 
            {
                action[Mario.KEY_JUMP] = true;
            }
            ++trueJumpCounter;
        }
        else 
        {            
            action[Mario.KEY_JUMP] = false;
            trueJumpCounter = 0;
        }
        
        //rallentamento corsa
        if (timer == 5)
        {
            action[Mario.KEY_RIGHT] = false;
            timer =0;
        }
        else 
        {
            action[Mario.KEY_RIGHT] = true;
            timer++;
        }
               
        AntiStop();   //se Mario sta fermo per troppo tempo, salta
        
        if (trueJumpCounter > 16) {
            trueJumpCounter = 0;
            action[Mario.KEY_JUMP] = false;
        }
        
        //se Mario è rosso spara continuamente
        if(marioMode == 2)
        {       
            if (action[Mario.KEY_SPEED] == true)   
            {
                action[Mario.KEY_SPEED] = false;
            }
            else action[Mario.KEY_SPEED] = true;   
        }
                    
        return action;
    }
    
}