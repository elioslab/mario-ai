package ms.marioai.agents.controllers.students2017;

import ms.marioai.agents.controllers.*;
import ms.marioai.agents.Agent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import ms.marioai.benchmark.mario.environments.Environment;

public class Picasso extends BasicMarioAIAgent implements Agent {

    public int savedMarioX;
    public int thisMarioX;
    public int counter;
    
    public Picasso() {
        
        super("Picasso");
        reset();
    }

    public void reset() {
        action = new boolean[Environment.numberOfKeys];
        action[Mario.KEY_RIGHT] = false;
        action[Mario.KEY_SPEED] = false;
        action[Mario.KEY_LEFT] = false;
        action[Mario.KEY_UP] = false;
        action[Mario.KEY_DOWN] = false;
        action[Mario.KEY_JUMP] = false;
        savedMarioX = marioEgoCol;
        counter = 50;
    }

    private boolean DangerOfAny() {

        if ((getReceptiveFieldCellValue(marioEgoRow + 3, marioEgoCol + 1) == 0
                && getReceptiveFieldCellValue(marioEgoRow + 2, marioEgoCol + 1) == 0)
                || getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 1) != 0
                || getReceptiveFieldCellValue(marioEgoRow, marioEgoCol + 2) != 0
                || getReceptiveFieldCellValue(marioEgoRow - 1, marioEgoCol + 1) != 0
                || getReceptiveFieldCellValue(marioEgoRow - 2, marioEgoCol + 1) != 0
                || getEnemiesCellValue(marioEgoRow, marioEgoCol + 1) != 0
                || getEnemiesCellValue(marioEgoRow, marioEgoCol + 2) != 0
                || getEnemiesCellValue(marioEgoRow, marioEgoCol + 3) != 0
                || getEnemiesCellValue(marioEgoRow - 1, marioEgoCol + 2) != 0
                || getEnemiesCellValue(marioEgoRow - 1, marioEgoCol + 3) != 0
                || getEnemiesCellValue(marioEgoRow - 1, marioEgoCol + 4) != 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean[] getAction() {

        action[Mario.KEY_RIGHT] = true;
                  thisMarioX = marioEgoCol;
            
        if(isMarioStuck())
        {
            action[Mario.KEY_JUMP] = true;
            action[Mario.KEY_SPEED] = true;
            action[Mario.KEY_RIGHT] = action[Mario.KEY_LEFT];
            action[Mario.KEY_LEFT] = !action[Mario.KEY_RIGHT];
        }
        else
        {
            action[Mario.KEY_JUMP] = false;
            action[Mario.KEY_SPEED] = true;
            action[Mario.KEY_RIGHT] = true;
            action[Mario.KEY_LEFT] = false;
                
        if( DangerOfAny() && isMarioAbleToJump  )
        {
               
            action[Mario.KEY_RIGHT] = true;
            action[Mario.KEY_SPEED] = true;
            action[Mario.KEY_JUMP] = true; 
            
            
            
        }
        
        else if (getEnemiesCellValue(marioEgoRow + 2  , marioEgoCol+4 ) != 0 || getEnemiesCellValue(marioEgoRow + 2  , marioEgoCol+6 ) !=0  )
        {
            action[Mario.KEY_SPEED] = false;
            
            
            
        }
       
        
         else if ( getReceptiveFieldCellValue(marioEgoRow + 1 , marioEgoCol) == 1 && isMarioAbleToJump )
        {
            action[Mario.KEY_JUMP] = false;
            action[Mario.KEY_SPEED] = true;
            action[Mario.KEY_RIGHT] = true;
        }
         
        else
        {
           
            action[Mario.KEY_SPEED] = action[Mario.KEY_JUMP] = isMarioAbleToJump || !isMarioOnGround;
            
        }
            
        }             
        return action;
    }
    
    public boolean isMarioStuck()
    {
        if(counter>0)
            counter--;
        else
        {
            counter = 50;
            if(savedMarioX == thisMarioX)
                return true;
            
        }
        return false;
                
    }

    @Override
    public void end() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void begin() {
    }
}
