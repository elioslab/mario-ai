package ms.marioai.agents.controllers;

import ms.marioai.agents.Agent;
import ms.marioai.benchmark.mario.environments.Environment;

import java.util.Random;

public class RandomAgent extends BasicMarioAIAgent implements Agent
{
    public RandomAgent()
    {
        super("RandomAgent");
        reset();
    }

    private Random R = null;

    public void reset()
    {
        R = new Random();
    }

    public boolean[] getAction()
    {
        boolean[] ret = new boolean[Environment.numberOfKeys];

        for (int i = 0; i < Environment.numberOfKeys; ++i)
        {
            boolean toggleParticularAction = R.nextBoolean();
            toggleParticularAction = (i == 0 && toggleParticularAction && R.nextBoolean()) ? R.nextBoolean() : toggleParticularAction;
            toggleParticularAction = (i == 1 || i > 3 && !toggleParticularAction) ? R.nextBoolean() : toggleParticularAction;
            toggleParticularAction = (i > 3 && !toggleParticularAction) ? R.nextBoolean() : toggleParticularAction;

            ret[i] = toggleParticularAction;
        }
        if (ret[1])
            ret[0] = false;
        return ret;
    }
}
