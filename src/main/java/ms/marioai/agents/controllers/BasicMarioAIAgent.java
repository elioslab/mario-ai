package ms.marioai.agents.controllers;

import ms.marioai.agents.Agent;
import ms.marioai.benchmark.mario.environments.Environment;

public class BasicMarioAIAgent implements Agent
{
    protected boolean action[] = new boolean[Environment.numberOfKeys];
    protected String name = "Instance_of_BasicAIAgent._Change_this_name";

    protected byte[][] levelScene;
    protected byte[][] enemies;
    protected byte[][] mergedObservation;
    protected float[] marioFloatPos = null;
    protected float[] enemiesFloatPos = null;
    protected int[] marioState = null;

    protected int marioStatus;
    protected int marioMode;
    protected boolean isMarioOnGround;
    protected boolean isMarioAbleToJump;
    protected boolean isMarioAbleToShoot;
    protected boolean isMarioCarrying;
    protected int getKillsTotal;
    protected int getKillsByFire;
    protected int getKillsByStomp;
    protected int getKillsByShell;

    protected int receptiveFieldWidth;
    protected int receptiveFieldHeight;
    protected int marioEgoRow;
    protected int marioEgoCol;

    public Environment environment = null;

    // values of these variables could be changed during the Agent-Environment interaction.
    // Use them to get more detailed or less detailed description of the level.
    // for information see documentation for the benchmark <link: marioai.org/marioaibenchmark/zLevels
    protected int zLevelScene = 2;
    protected int zLevelEnemies = 2;

    public BasicMarioAIAgent(String s) { setName(s); }
    
    public boolean[] getAction() { return new boolean[Environment.numberOfKeys]; }

    public void integrateObservation(Environment environment) {
        this.environment = environment;

        levelScene = environment.getLevelSceneObservationZ(zLevelScene);
        enemies = environment.getEnemiesObservationZ(zLevelEnemies);
        mergedObservation = environment.getMergedObservationZZ(zLevelScene, zLevelEnemies);

        this.marioFloatPos = environment.getMarioFloatPos();
        this.enemiesFloatPos = environment.getEnemiesFloatPos();
        this.marioState = environment.getMarioState();

        receptiveFieldWidth = environment.getReceptiveFieldWidth();
        receptiveFieldHeight = environment.getReceptiveFieldHeight();

        marioStatus = marioState[0];
        marioMode = marioState[1];
        isMarioOnGround = marioState[2] == 1;
        isMarioAbleToJump = marioState[3] == 1;
        isMarioAbleToShoot = marioState[4] == 1;
        isMarioCarrying = marioState[5] == 1;
        getKillsTotal = marioState[6];
        getKillsByFire = marioState[7];
        getKillsByStomp = marioState[8];
        getKillsByShell = marioState[9];
    }

    public void giveIntermediateReward(float intermediateReward) { int i=0; }

    public void reset() {
        action = new boolean[Environment.numberOfKeys];
    }

    public void setObservationDetails(final int rfWidth, final int rfHeight, final int egoRow, final int egoCol) {
        receptiveFieldWidth = rfWidth;
        receptiveFieldHeight = rfHeight;
        marioEgoRow = egoRow;
        marioEgoCol = egoCol;
    }

    @Deprecated
    public boolean[] getAction(Environment observation) {
        return new boolean[Environment.numberOfKeys]; // Empty action
    }

    public String getName() { return name; }
    public void setName(String Name) { this.name = Name; }

    public int getEnemiesCellValue(int x, int y) {
        if (x < 0 || x >= levelScene.length || y < 0 || y >= levelScene[0].length)
            return 0;
        return enemies[x][y];
    }

    public int getReceptiveFieldCellValue(int x, int y) {
        if (x < 0 || x >= levelScene.length || y < 0 || y >= levelScene[0].length)
            return 0;

        return levelScene[x][y];
    }

    @Override
    public void episodeEnd() {    }

    @Override
    public void beginEpisode() {    }

    @Override
    public void end() { }

    @Override
    public void begin() { }
    
    @Override
    public String print() { return "no info"; }
}
