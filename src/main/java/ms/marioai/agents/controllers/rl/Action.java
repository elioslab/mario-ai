package ms.marioai.agents.controllers.rl;

class Action {
    private Boolean[] values;
      
    Action(Boolean[] values) {
        this.values = values;
    }

    public Boolean[] getValues() {
        return values;
    }
}
