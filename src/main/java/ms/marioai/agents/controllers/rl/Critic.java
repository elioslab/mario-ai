package ms.marioai.agents.controllers.rl;

import ms.marioai.benchmark.mario.environments.Environment;
import ms.marioai.tools.EvaluationInfo;

public class Critic {
    private Environment environment;
    private Integer distance = -1;
    private Integer collisions = 0;
    private Integer coins = 0;

    public Critic() {
    }
    
    public Double lose() {
        Double punish_for_lose = -20.0;
        return punish_for_lose;
    }
    
    public Double win() {
        Double reward_for_win = 20.0;
        return reward_for_win;
    }
    
    public Double evaluate()  {
        Double fitness = 0.0;
        
        EvaluationInfo evaluation = environment.getEvaluationInfo();
        
        int value = evaluation.distancePassedPhys - distance;
        fitness = (double)value - 1.0;
        
        Double punish_for_collision = -50.0;
        if((collisions - evaluation.collisionsWithCreatures) != 0)
            fitness = punish_for_collision;
        
        distance = evaluation.distancePassedPhys;
        collisions = evaluation.collisionsWithCreatures;
        
        return fitness;
    }

    public void update(Environment environment) {
        this.environment = environment;
    }
}
