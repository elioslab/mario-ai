package ms.marioai.agents.controllers.rl;

import ms.marioai.agents.Agent;
import ms.marioai.agents.controllers.BasicMarioAIAgent;
import ms.marioai.benchmark.mario.engine.sprites.Mario;
import java.util.ArrayList;
import java.util.List;
import wondertech.learner.Learner;
import wondertech.learner.Status;
import wondertech.qfunctions.QFunctionType;

public class RLAgent extends BasicMarioAIAgent implements Agent {
    private Learner learner;
    private Critic critic;
    private final List<Action> actions;
    
    private final Integer mario_view_width = 5;
    private final Integer mario_view_height = 5;
    private final Integer mario_status_size = 0;
    
    private QFunctionType type = QFunctionType.table;
    private Double alfa = 0.1;
    private final Double gamma = 0.99;
    private Double kappa = 1.0;
    private final Double delta_kappa  = 0.01;
    private final Integer memory_size  = 5;
    private Boolean exploit = false;
    
    public RLAgent() {
        super("ReinforcementMarioAIAgent");
        
        actions = new ArrayList<>();

        //Boolean[] nope = new Boolean[] { false, false, false, false, false, false };
        //actions.add(new Action(nope));

        Boolean[] left = new Boolean[] { false, false, false, false, false, false };
        left[Mario.KEY_LEFT] = true;
        actions.add(new Action(left));
        
        Boolean[] right = new Boolean[] { false, false, false, false, false, false };
        right[Mario.KEY_RIGHT] = true;
        actions.add(new Action(right));
        
        //Boolean[] down = new Boolean[] { false, false, false, false, false, false };
        //down[Mario.KEY_DOWN] = true;
        //actions.add(new Action(down));
        
        //Boolean[] jump = new Boolean[] { false, false, false, false, false, false };
        //jump[Mario.KEY_JUMP] = true;
        //actions.add(new Action(jump));
        
        Boolean[] jump_right = new Boolean[] { false, false, false, false, false, false };
        jump_right[Mario.KEY_JUMP] = true;
        jump_right[Mario.KEY_RIGHT] = true;
        actions.add(new Action(jump_right));
        
        Boolean[] jump_left = new Boolean[] { false, false, false, false, false, false };
        jump_left[Mario.KEY_JUMP] = true;
        jump_left[Mario.KEY_LEFT] = true;
        actions.add(new Action(jump_left)); 
    }
         
    @Override
    public boolean[] getAction() {
        boolean[] output = new boolean[] { false, false, false, false, false, false };
        critic.update(environment);
               
        byte[][] observations = environment.getMergedObservationZZ(0, 0);
        int[] mario = environment.getMarioState();
       
        Status status = new Status(environment.getMergedObservationZZ(0, 0), mario_view_width, mario_view_height);
        status.add(environment.getMarioState());
       
        learner.memorize(status, critic.evaluate());
        learner.update();

        int index = learner.next(status);
        
        Boolean[] action = actions.get(index).getValues();
        for(Integer i=0; i<output.length; i++)
            output[i] = action[i];
        
        return output;
    }
      
    @Override
    public void beginEpisode() { 
        learner.beginEpisode(); 
    }
    
    @Override
    public void episodeEnd() { 
        learner.endEpisode(); 
    }
    
    @Override
    public void begin() { 
        critic = new Critic();
        learner = new Learner(actions.size(), mario_status_size + mario_view_width * mario_view_height, type, alfa, gamma, kappa, delta_kappa, memory_size, exploit);
        learner.load();
    }
    
    @Override
    public void end() { learner.save(); }
    
    @Override
    public String print() {  return learner.print(); }
}