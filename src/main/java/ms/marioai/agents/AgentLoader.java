package ms.marioai.agents;

import ms.marioai.agents.controllers.human.HumanKeyboardAgent;
import ms.marioai.utils.wox.serial.Easy;

import java.io.IOException;

public final class AgentLoader
{
    private static final AgentLoader _instance = new AgentLoader();

    private AgentLoader() {}

    public static AgentLoader getInstance()
    {
        return _instance;
    }

    public Agent loadAgent(String name, boolean isPunj)
    {
        Agent agent = null;

        try
        {
            agent = (Agent) Class.forName(name).newInstance();
        } 
        catch (ClassNotFoundException e)
        {
            System.out.println("[~ Mario AI ~] :" + name + " is not a class name; trying to load a wox definition with that name.");
            
            try
            {
                agent = (Agent) Easy.load(name);
            } 
            catch (Exception ex)
            {
                System.err.println("[~ Mario AI ~] :" + name + " is not a wox definition");
                agent = null;
            }

            if (agent == null)
            {
                System.err.println("[~ Mario AI ~] : wox definition has not been found as well. Loading <HumanKeyboardAgent> instead");
                agent = new HumanKeyboardAgent();
            }
            
            System.out.println("[~ Mario AI ~] : agent = " + agent);
        } 
        catch (Exception e)
        {
            agent = new HumanKeyboardAgent();
            System.err.println("[~ Mario AI ~] : Agent is null. Loading agent with name " + name + " failed.");
            System.out.println("Agent has been set to default: " + agent);
        }

        return agent;
    }
}
