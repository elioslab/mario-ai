package ms.marioai.scenarios;

import ms.marioai.benchmark.tasks.BasicTask;
import ms.marioai.tools.EvaluationInfo;
import ms.marioai.tools.MarioAIOptions;

public final class TestMain
{
    public static void main(String[] args)
    {
        String param = " -ll 512 -ls 0 -vis on -ld 0 -lt 0";
        //Competitor c = new Competitor("HumanKeyboardAgent", "-ag ms.marioai.agents.controllers.human.HumanKeyboardAgent");
        //Competitor c = new Competitor("ForwardAgent", "-ag ms.marioai.agents.controllers.ForwardAgent");
        Competitor c = new Competitor("RandomAgent", "-ag ms.marioai.agents.controllers.RandomAgent");
        //Competitor c = new Competitor("ScaredAgent", "-ag ms.marioai.agents.controllers.ScaredAgent");
        //Competitor c = new Competitor("NomeCognome", "-ag ms.marioai.agents.controllers.students2017.NomeCognome");
        //Competitor c = new Competitor("AStarAgent", "-ag ms.marioai.agents.astar.AStarAgent");
        
        String run = c.agent + param;
        MarioAIOptions marioAIOptions = new MarioAIOptions(run);
        
        marioAIOptions.setReceptiveFieldHeight(22);
        marioAIOptions.setReceptiveFieldWidth(22);
        
        BasicTask basicTask = new BasicTask(marioAIOptions);
        basicTask.setOptionsAndReset(marioAIOptions);
        basicTask.doEpisodes(1,false,1);
        EvaluationInfo result = basicTask.getEvaluationInfo();
        c.performace += (float)result.distancePassedCells / result.levelLength;
        c.fitness +=  result.computeBasicFitness();
        System.out.println("    - " + c.name + " distancePassedCells=" + result.distancePassedCells + " fitness=" + result.computeBasicFitness());

        System.exit(0);
    }
}