package ms.marioai.scenarios;

import ms.marioai.benchmark.tasks.BasicTask;
import ms.marioai.tools.EvaluationInfo;
import ms.marioai.tools.MarioAIOptions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class CompetitionMain
{
    public static void main(String[] args)
    {
        List<Competitor> competitors = new ArrayList<>();
        
        competitors.add(new Competitor("Forward_Agent", "-ag ms.marioai.agents.controllers.ForwardAgent"));
        competitors.add(new Competitor("Scared_Agent", "-ag ms.marioai.agents.controllers.ScaredAgent"));
        competitors.add(new Competitor("Assanti_Agemt-2016", "-ag ms.marioai.agents.controllers.students2016.Assanti"));        
        competitors.add(new Competitor("Beccaria1_Agent-2016", "-ag ms.marioai.agents.controllers.students2016.Beccaria_1"));
        competitors.add(new Competitor("Beccaria2_Agent-2016", "-ag ms.marioai.agents.controllers.students2016.Beccaria_2"));        
        competitors.add(new Competitor("Cipollini_Agent-2016", "-ag ms.marioai.agents.controllers.students2016.Cipollini"));
        competitors.add(new Competitor("Conte_Agent-2016", "-ag ms.marioai.agents.controllers.students2016.Conte"));
        competitors.add(new Competitor("Mollo_Agent-2016", "-ag ms.marioai.agents.controllers.students2016.Mollo"));
        competitors.add(new Competitor("PiaDaum_Agent-2016", "-ag ms.marioai.agents.controllers.students2016.PiaDaum"));
        competitors.add(new Competitor("Sanetti_Agent-2016", "-ag ms.marioai.agents.controllers.students2016.Sanetti"));
        competitors.add(new Competitor("Vadala_Agent-2016", "-ag ms.marioai.agents.controllers.students2016.Vadala"));
        competitors.add(new Competitor("Viviani_Agent-2016", "-ag ms.marioai.agents.controllers.students2016.Viviani"));
        competitors.add(new Competitor("DeMicheli_Agent-2017", "-ag ms.marioai.agents.controllers.students2017.BenedettaDemicheli"));
        competitors.add(new Competitor("Bosica1_Agent-2017", "-ag ms.marioai.agents.controllers.students2017.FabioBosica_Mushrooms"));
        competitors.add(new Competitor("Bosica2_Agent-2017", "-ag ms.marioai.agents.controllers.students2017.FabioBosica_Turtles"));
        competitors.add(new Competitor("Garrone_Agent-2017", "-ag ms.marioai.agents.controllers.students2017.Garrone"));
        competitors.add(new Competitor("MatteoArcelloni_Agent-2017", "-ag ms.marioai.agents.controllers.students2017.MatteoArcelloni"));
        competitors.add(new Competitor("Picasso_Agent-2017", "-ag ms.marioai.agents.controllers.students2017.Picasso"));
               
        Integer size = 20;
        Integer verbose = 1;

        Integer difficulty = 0;
        Integer level = 0;

        for(int i=0; i<size; i++)      
        {
            Integer seed = (int)(Math.random() * (256-1)+1);
            Integer length = 512;

            String param = " -ll " + length + " -ls " + seed + " -vis off -ld " + difficulty + " -lt " + level;
            //String param = " -ll " + length + " -ls " + seed + difficulty + " -lt " + level;
            
            if(verbose == 1)
                System.out.println(i + " - length=" + length + " seed=" + seed);
            
            for(int j=0; j<competitors.size(); j++)
            {
                Competitor c = competitors.get(j);
                String run = c.agent + param;
                MarioAIOptions marioAIOptions = new MarioAIOptions(run);
                marioAIOptions.setReceptiveFieldHeight(22);
                marioAIOptions.setReceptiveFieldWidth(22);
                BasicTask basicTask = new BasicTask(marioAIOptions);
                basicTask.setOptionsAndReset(marioAIOptions);
                basicTask.getAgent().begin();
                basicTask.doEpisodes(1,false,1);
                EvaluationInfo result = basicTask.getEvaluationInfo();
                c.performace += (float)result.distancePassedCells / result.levelLength;
                c.fitness +=  result.computeBasicFitness();
                if (verbose == 1) { System.out.println("    - " + c.name + " distancePassedCells=" + result.distancePassedCells + " fitness=" + result.computeBasicFitness()); }
                basicTask.getAgent().begin();
            }
            
            System.out.println();
        }
        
        Collections.sort(competitors, (c1, c2) -> c1.performace > c2.performace ? -1 : (c1.performace > c2.performace) ? 1 : 0);
        
        System.out.println("RESULTS");
        
        competitors.stream().forEach((c) -> { System.out.println(c.name + " = " + c.performace/size + " (" + c.fitness + ")"); });
        System.exit(0);
    }
}