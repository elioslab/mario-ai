package ms.marioai.scenarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import ms.marioai.benchmark.tasks.BasicTask;
import ms.marioai.tools.EvaluationInfo;
import ms.marioai.tools.MarioAIOptions;
import java.util.ArrayList;
import java.util.List;
import org.LiveGraph.dataFile.write.DataStreamWriter;
import org.LiveGraph.dataFile.write.DataStreamWriterFactory;
import org.apache.commons.collections4.queue.CircularFifoQueue;

public final class ReinforcementLearningMain {
    
    public static void main(String[] args) throws IOException {
        if(Files.exists(Paths.get("./stream.lgdat")))
            Files.delete(Paths.get("./stream.lgdat"));
        DataStreamWriter stream = DataStreamWriterFactory.createDataWriter("./", "stream");
        stream.setSeparator(";");
        stream.writeFileInfo("Super Mario AI file.");
        stream.addDataSeries("Performance");
        
        List<Competitor> competitors = new ArrayList<>();
        competitors.add(new Competitor("RLAgent", "-ag ms.marioai.agents.controllers.rl.RLAgent"));
        Competitor c = competitors.get(0);
        
        MarioAIOptions options = new MarioAIOptions(c.agent);
        options.setReceptiveFieldHeight(22);
        options.setReceptiveFieldWidth(22);
        options.setTimeLimit(300);
        options.setMarioInvulnerable(false);
        options.setLevelDifficulty(0);
        options.setLevelLength(512);
        options.setLevelType(0);
        options.setFPS(24);
        options.setGameViewer(false);
        options.setReceptiveFieldVisualized(false);
        options.setVisualization(false);
        
        BasicTask task = new BasicTask(options);
        task.getAgent().begin();
        
        Integer runs = 100;
        Integer window = 1;
        CircularFifoQueue<Float> cumulative = new CircularFifoQueue<>(window);
        for(int i=1; i<=runs; i++) {
            Integer seed = (int)(Math.random() * (256-1)+1);
            options.setLevelRandSeed(seed);
            task.setOptionsAndReset(options);

            task.doEpisodes(1,false,1);

            EvaluationInfo result = task.getEvaluationInfo();
            c.performace = (float)result.distancePassedCells / result.levelLength;
            cumulative.add(c.performace);
            float average = 0;
            for(int j=0; j<cumulative.size(); j++)
                average += cumulative.get(j);
            average /= cumulative.size();

            System.out.println("run=" + i + " Performance=" + average);
            System.out.println(task.getAgent().print());
            System.out.println();

            stream.setDataValue(average);
            stream.writeDataSet();
        }
        task.getAgent().end();
        stream.close();
        System.exit(0);
    }
}
