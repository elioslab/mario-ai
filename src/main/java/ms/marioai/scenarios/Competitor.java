package ms.marioai.scenarios;

public class Competitor
{
    public String name;
    public String agent;
    public float performace = 0.0f;
    public float fitness = 0.0f;

    public Competitor(String name, String agent) 
    {
        this.name = name;
        this.agent = agent;
    }
}
